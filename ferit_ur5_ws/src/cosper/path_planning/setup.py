from setuptools import setup

setup(
    name='path_planning',
    version='0.0.0',
    packages=['path_planning'],
    package_dir={'': 'src'},
)